package com.twuc.webApp.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PathValueControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_path_value1() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().isOk())
                .andExpect(content().string("The book for user 2"));
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(status().isOk())
                .andExpect(content().string("The book for user 23"));
    }


    @Test
    void should_nearest_match() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("direct"))
                .andExpect(status().isOk());
    }

    @Test
    void test_question_mark() throws Exception {
        mockMvc.perform(get("/api/find/s"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/find/"))
                .andExpect(status().isNotFound());
        mockMvc.perform(get("/api/find/mark"))
                .andExpect(status().isNotFound());
    }

    @Test
    void test_multiplication_symbol() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(status().isOk())
                .andExpect(content().string("*"));
        mockMvc.perform(get("/api/wildcards/before/abc/after"))
                .andExpect(status().isOk())
                .andExpect(content().string("before*after"));
        mockMvc.perform(get("/api/*test"))
                .andExpect(status().isOk())
                .andExpect(content().string("*test"));
        mockMvc.perform(get("/api/test*"))
                .andExpect(status().isOk())
                .andExpect(content().string("test*"));
    }


    @Test
    void use_all_and_all_maching() throws Exception {
        mockMvc.perform(get("/api/match/user/userId/abc"))
                .andExpect(status().isOk());
    }

    @Test
    void use_regix() throws Exception {
        mockMvc.perform(get("/api/regix/111"))
                .andExpect(status().isOk())
                .andExpect(content().string("hello Regix"));
    }

    @Test
    void query_string() throws Exception {
        mockMvc.perform(get("/api/query")
                .param("id", "10"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/query"))
                .andExpect(status().isBadRequest());
    }


}
