package com.twuc.webApp.controller;

import jdk.net.SocketFlow;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
public class PathValueController {
    @GetMapping("/api/users/{userId}/books")
    public String getString(@PathVariable Integer userId) {
        return "The book for user " + userId;
    }

    @GetMapping("/api/segments/good")
    public String directGetValue() {
        return "direct";
    }

    @GetMapping("/api/segments/{segmentName}")
    public String indirectGetValue() {
        return "indirect";
    }

    @GetMapping("/api/find/?")
    public String isFindByRount() {
        return "find it";
    }

    @GetMapping("/api/wildcards/*")
    public String wildcardsAnything() {
        return "*";
    }

    @GetMapping("/api/wildcards/before/*/after")
    public String beforeAfter() {
        return "before*after";
    }

    @GetMapping("/api/*test")
    public String test() {
        return "*test";
    }

    @GetMapping("/api/test*")
    public String testTwo() {
        return "test*";
    }


    @GetMapping("/api/match/**/abc")
    public String getAllMatch() {
        return "abc";
    }

    @GetMapping("/api/regix/{[0-9]{3}}")
    public String getRegix() {
        return "hello Regix";
    }

    @GetMapping(value = "api/query", params = {"!name"})
    public String getQuery(@RequestParam("id") Integer id) {
        return "getQuery";
    }
    @PostMapping("/api/response")
//    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity GetStatus(){
        return ResponseEntity.created(null)
                .contentType(MediaType.APPLICATION_JSON)
                .build();
    }
}
